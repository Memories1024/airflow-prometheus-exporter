import logging
import os
import yaml


CONFIG_FILE = os.environ["AIRFLOW_HOME"] + "/prometheus_config.yml"


def load_xcom_config():
    """Loads the XCom config if present."""
    try:
        with open(CONFIG_FILE) as file:
            # The FullLoader parameter handles the conversion from YAML
            # scalar values to Python the dictionary format
            return yaml.load(file, Loader=yaml.FullLoader)
    except FileNotFoundError:
        logging.error("can't load config file for prometheus xcoms")
        return {}
